<br>
<div class="container">
<div class="row">
<hr>
<h3>FORMULARIO A EDITAR:</h3>
<hr>
<div class="col-md-12 " >

<form action="<?php echo site_url(); ?>/clientes/procesarActualizacion"
  method="post"
  >
  <input type="hidden" name="id_cli" id="id_cli"value="<?php echo $cliente->id_cli; ?>">
  <div style="border:3px soli #CDD1C3; with:40%; margin-top:4px; margin-left: 7%; margin-right: 7%">
  <br>
  <br>
  <label for="">PAIS</label>
  <select class="form-control" style="color:#000000" name="fk_id_pais" id="fk_id_pais"
  required>
    <option value="">----Seleccione un pais----</option>
    <?php if($listadoPaises): ?>
    <?php foreach ($listadoPaises->result() as $paisTemporal): ?>
      <option value="<?php echo $paisTemporal->id_pais ?>">
        <?php echo $paisTemporal->nombre_pais; ?>
      </option>
    <?php endforeach; ?>
  <?php endif; ?>
  </select>

    <br>
    <br>
    <label for="">IDENTIFICACIÓN</label>
    <input class="form-control" value="<?php echo $cliente->identificacion_cli; ?>" required="required"  type="number" name="identificacion_cli" id="identificacion_cli" placeholder="Por favor Ingrese la Identificacion" >
    <br>
    <br>
    <label for="">APELLIDO</label>
    <input class="form-control" value="<?php echo $cliente->apellido_cli; ?>" required="required"  type="text" name="apellido_cli" id="apellido_cli" placeholder="Por favor Ingrese el apellido">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control" value="<?php echo $cliente->nombre_cli; ?>" required="required"  type="text" name="nombre_cli" id="nombre_cli" placeholder="Por favor Ingrese el nombre">
    <br>
    <br>
    <label for="">TELEFONO</label>
    <input class="form-control" value="<?php echo $cliente->telefono_cli; ?>" required="required"  type="number" name="telefono_cli" id="telefono_cli" placeholder="Por favor Ingrese el telefono">
    <br>
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control" value="<?php echo $cliente->direccion_cli; ?>" required="required"  type="text" name="direccion_cli" id="direccion_cli" placeholder="Por favor Ingrese la dirección">
    <br>
    <br>
    <label for="">CORREO ELECTRÓNICO</label>
    <input class="form-control"value="<?php echo $cliente->email_cli; ?>" required="required"   type="email" name="email_cli" id="email_cli" placeholder="Por favor Ingrese el correo">
    <br>
    <br>
    <label for="">ESTADO</label>
    <select style="color:#000000" class="form-control" name="estado_cli" id="estado_cli">
        <option value="">--Seleccione--</option>
        <option value="ACTIVO">ACTIVO</option>
        <option value="INACTIVO">INACTIVO</option>
    </select>
    <br>
    <button type="submit" name="button" class="btn btn-primary">
      ACTUALIZAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/clientes/index"
      class="btn btn-warning">
    <i class="fa fa-times"></i> CANCELAR
    </a>
  </div>
</form>
</div>
</div>
</div>
</div>

<script type="text/javascript">
  //Sctivando el pais selecionado para el clientes
  $("#fk_id_pais").val("<?php echo $cliente->fk_id_pais; ?>");
  $("#estado_cli").val("<?php echo $cliente->estado_cli; ?>");
</script>
