<br>
<div class="container">
<div class="row">
<hr>
<h3>FORMULARIO:</h3>
<hr>
<div class="col-md-12 " >

<form action="<?php echo site_url(); ?>/clientes/guardarCliente"
  method="post" id="frm_nuevo_cliente"
  >
  <div style="border:3px soli #CDD1C3; with:40%; margin-top:4px; margin-left: 7%; margin-right: 7%">
  <br>
  <br>
  <label for="">PAIS</label>
  <select class="form-control" name="fk_id_pais" id="fk_id_pais"
  required>
    <option value="">----Seleccione un pais----</option>
    <?php if($listadoPaises): ?>
    <?php foreach ($listadoPaises->result() as $paisTemporal): ?>
      <option value="<?php echo $paisTemporal->id_pais ?>">
        <?php echo $paisTemporal->nombre_pais; ?>
      </option>
    <?php endforeach; ?>
  <?php endif; ?>
  </select>

    <br>
    <br>
    <label for="">IDENTIFICACIÓN</label>
    <input class="form-control" required="required"  type="number" name="identificacion_cli" id="identificacion_cli" placeholder="Por favor Ingrese la Identificacion" >
    <br>
    <br>
    <label for="">APELLIDO</label>
    <input class="form-control" required="required"  type="text" name="apellido_cli" id="apellido_cli" placeholder="Por favor Ingrese el apellido">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control" required="required"  type="text" name="nombre_cli" id="nombre_cli" placeholder="Por favor Ingrese el nombre">
    <br>
    <br>
    <label for="">TELEFONO</label>
    <input class="form-control" required="required"  type="number" name="telefono_cli" id="telefono_cli" placeholder="Por favor Ingrese el telefono">
    <br>
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control" required="required"  type="text" name="direccion_cli" id="direccion_cli" placeholder="Por favor Ingrese la dirección">
    <br>
    <br>
    <label for="">CORREO ELECTRÓNICO</label>
    <input class="form-control" required="required"   type="email" name="email_cli" id="email_cli" placeholder="Por favor Ingrese el correo">
    <br>
    <br>
    <label for="">ESTADO</label>
    <select class="form-control" name="estado_cli" id="estado_cli">
        <option value="">--Seleccione--</option>
        <option value="ACTIVO">ACTIVO</option>
        <option value="INACTIVO">INACTIVO</option>
    </select>
    <br>
    <br>
    <label for="">FOTOGRAIFA</label>
    <input type="file" name="foto_cli" id="foto_cli" value="">
    <br>
    <br>
    <button type="submit" name="button" class="btn btn-primary">
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/clientes/index"
      class="btn btn-warning">
      <i class="fa fa-times"></i> CANCELAR
    </a>
  </div>
</form>
</div>
</div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_cliente").validate({
      rules:{
        fk_id_pais:{
          required:true
        },
        identificacion_cli:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        telefono_cli:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
      },
      apellido_cli:{
        letras:true,
        required:true
      }
      },
      messages:{
        fk_id_pais:{
          required:"Por favor seleccione el pais"
        },
        identificacion_cli:{
          required:"Por favor ingrese el número de cédula",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"La cédula solo acepta números"
        },
        telefono_cli:{
          required:"Por favor ingrese el número de teléfono celular",
          minlength:"El número de celular debe tener mínimo 10 digitos",
          maxlength:"El número de celular debe tener máximo 10 digitos",
          digits:"El número de celular solo acepta números"
        },
        apellido_cli:{
          letras:"Ingrese solamente letras",
          required:"Ingrese el apellido solo con letras"
        }
      }
    });
</script>
